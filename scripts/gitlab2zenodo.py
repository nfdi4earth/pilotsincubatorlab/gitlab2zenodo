import requests
import json
import yaml
import sys
import os
import re
class GitLab2ZenodoNFDI:
    """
    GitLab2ZenodoNFDI will push your code to zenodo
    Arguments are:
        --help,-help,-man
        -c: create a new project on zenoodo
        -m: parse a .cff file and upload the zenodo meta data
        -f: upload files
        -s: use the zenodo sandbox 
        -i: read zenodo doi from CITATION.cff identifyer type=doi, description=DOI of this project
    """
    helptext="GitLab2ZenodoNFDI will push your data to zenodo by use the zenodo_token and zenodo_record enviorment variable.\nArguments are:\n\t-c:\t create new project on zenodon\n\t-m:\t parse the .cff file and upload meta data to zenodo (eg. -m CITATION.cff)\n\t-f:\t upload the files if they exists(eg. -f ./A.txt path/to/B.csv)\n\t-s:\t publish to sandbox\n\t-i:\t reads zenodo identifier from .cff file"
    def create_new(self):
        """
        Create a new zenodo project and set the project id to it
        """
       
        self.repro = requests.post("/".join([self.zenodo_url,'api/deposit/depositions']),
                  params=self.params,
                  json={},
                  headers=self.headers).json()
        self.id=str(self.repro["id"])
        print("Created new zenodo project: "+str(self.id))
        self.bucket_url=self.repro["links"]["bucket"]
        return self.id
    def switch_sandbox(self,sandbox=False):
        """
        Switch between sandbox and public zenodo
        """
        print("Switch sanbox "+str(sandbox))
        if(sandbox):
            self.zenodo_url="https://sandbox.zenodo.org"
        else:
            self.zenodo_url="https://zenodo.org/"

    def __init__(self,acess,id=""):
        self.switch_sandbox(False)
        self.acess_token=acess
        self.params={'access_token': self.acess_token}
        self.headers={"Content-Type": "application/json"}
        self.id=id
        self.repro={}

    def catch_response_error(self,r):
        """
        Print wrong response error message
        """
        if r.status_code>399 :
            print(r.json()["message"])

    def set_zenodo_record_id(self,id):
        """
        Set the zenodo project id if it exists. If not create a new project with a new id.
        """
        r = requests.get("/".join([self.zenodo_url,"/api/deposit/depositions/%s?access_token=%s"% (id,self.acess_token)]))
        if id is not None and len(str(id))>1:
            self.id=-1
            r = requests.get("/".join([self.zenodo_url,"/api/deposit/depositions"]),params=self.params)
            tmp=r.json()
            for e in tmp:
                 if str(e["id"])==str(id):
                    self.repro=e
                    self.id=id
                    print("setting zenodo_record id to: "+id)
            if "links" in self.repro and "bucket" in self.repro["links"]:
                self.bucket_url=self.repro["links"]["bucket"]
            elif "files" in self.repro and len(self.repro["files"])>0:
            #if no buget link is provided but files are uploades bucket link is reconstructed from file download link
                download = self.repro["files"][0]['links']['download']
                self.bucket_url=os.sep.join(download.split(os.sep)[0:-1])
            else:
                print("No bucket link starting first file upload?")
                self.bucket_url=""
        if(self.id==-1):
            print("Can't find id zenodo project: "+str(id))
            self.create_new()
           


    def change_meta(self,meta):
        """
        Upload the meta json
        """
        print("Uploading meta ...")
        r2 = requests.post("/".join([self.zenodo_url,"api/deposit/depositions/%s/actions/edit?access_token=%s"] )% (str(self.id),self.acess_token))
        url="%s" % "/".join([self.zenodo_url,"api/deposit/depositions/%s?access_token=%s"] )% (str(self.id),self.acess_token)
        if "links" in self.repro and "bucket" in self.repro["links"]:
         url=self.repro["links"]["latest_draft"]
        metadatJ=json.dumps(meta)
        print(metadatJ)
        headers = {"Content-Type": "application/json"}
        r = requests.put(url,
            data=metadatJ,
            headers=headers
        )
        self.catch_response_error(r)

    def update_files(self, file_list):
        """
        upload all files in the list if they exists
        """
        print("Updateing files ...")
        for f in file_list:
            if self.bucket_url=="":
                data = {'name': f.split(os.sep)[-1]}
                files = {'file': open(f, 'rb')}
                r = requests.post("/".join([self.zenodo_url,'api/deposit/depositions/%s/files'])% self.id,params=self.params, data=data,files=files)
            else:
                r = requests.put(
                "%s/%s" % (self.bucket_url, f.split(os.sep)[-1]),
                data=open(f, 'rb'),
                params=self.params
                )
                self.catch_response_error(r)
    def get_id_from_cff(path):
         with open(path, "r") as stream:
            try:
                x=yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
            for r in x['identifiers']:
                if r['type']=="doi" and r['description']=="DOI of this project":
                    id=r["value"].split(".")[-1]
                    print("set id from .cff: "+id) 
                    return id
    def read_cff(self, path):
        """
        Read .cff file and parse it to zenodo meta json
        """
        
        x={}
        with open(path, "r") as stream:
            try:
                x=yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
        z={}
        z['title']=x['title']
        z['creators']=[]
        for i in range(len(x['authors'])):
            a =x['authors'][i]
            z['creators'].append({})
            z['creators'][-1]['affiliation']=a['affiliation']
            if 'orcid'in a or i<1:
                z['creators'][-1]['orcid']=a['orcid'][18:]
            z['creators'][-1]['name']=a['family-names']+" , "+a['given-names']
        z['keywords']=x['keywords']
        z['description']=x['abstract']
        z['upload_type']=x['type']
        z['communities']=[]
        z['subjects']=[]
        if "identifiers" not in x:
            print("Pleas add at least the identifier for on eresearch fild and your doi")
            exit(0)
        for r in x['identifiers']:
            if r['type']=="url":
                z['subjects'].append({"term":r["description"],"identifier": r['value'],"scheme": "url"})
            elif r['type']=="doi":
                z['subjects'].append({"term":r["description"],"identifier": r['value'],"scheme": "doi"})
        if(isinstance(x["license"], str)):
            z["license"]={"id": x["license"]}
        elif(isinstance(x["license"],list)):
            z["license"]={"id": x["license"][0]}
            
        z['language']="eng"
        z["references"]=[]
        if "references" in x:
            for r in x["references"]:
                #Store extra information like community or research fiel in zenodo subjects
                if "scope" in r and r["scope"]=="zenodo community":
                    z['communities'].append({"identifier":r["title"]})
                    continue
                if "scope" in r and r["scope"]=="zenodo subject":
                    z['subjects'].append({"term":r["title"],"identifier": r['url'],"scheme": "url"})
                    continue
                name=""
                if len(r["authors"])<4:
                    for i in range(len(r["authors"])):
                        a=r["authors"][i]
                        name+=a["name"]
                    if i<len(r["authors"])-2:
                        name+="; "
                    elif i<len(r["authors"])-1:
                        name+=" and "
                else:
                    a=r["authors"][0][0]
                    name+=a["name"]+ " et al"
                if "year" in r:
                    ref=name+".("+str(r["year"])+")"
                
                ref+=". "+r["title"]
                if "doi" in r:
                    ref+=". DOI:" +r["doi"]   
                elif "journal" in r:
                    ref+=". Journal: "+r["journal"]
                z["references"].append(ref)
        meta={}
        meta["metadata"]=z
        return meta
    
if __name__ == '__main__':
    #LYx4QGHHNTX6S7XWhPfpx6aG5dhxotXId7Ou1VEuJW3FgzXPTXJgso33YqHb
    args=sys.argv
    acess_token = os.environ.get('zenodo_token') # None
    zenodo_id = os.environ.get('zenodo_record') # None
    if("--help "in args or "-help" in args or "-man" in args):
        print(GitLab2ZenodoNFDI.helptext)
        exit(1)
    if acess_token is None:
        print("No zenodo_token is defined. Please define zenodo_token enviormnent variable")
        exit(0)
    if("-i" in args):
            i=args.index("-i")+1
            if i>=len(args):
                print("-i musst follod from the CITATION.cff path")
            elif not os.path.isfile(args[i]):
                print("-i is no file"+args[i])
            else:    
               zenodo_id=GitLab2ZenodoNFDI.get_id_from_cff(args[i])
    parser=GitLab2ZenodoNFDI(id=zenodo_id,acess =acess_token )
   
    if("-s" in args):
        parser.switch_sandbox(True)
    parser.set_zenodo_record_id(zenodo_id)
    if "-c" in args or parser.id is None:
        parser.create_new()
      
    if "-f" in args:
        i=args.index("-f")+1
        files=[]
        while(i<len(args)):
            if args[i] in ["-m","-c","-help","--help","-f","-s","-man","-i"]:
                break
            if os.path.isfile(args[i]):
                files.append(args[i])
            i+=1
        if not files:
              print("Cant load file list.\n After -f at least one file path must follow")
        elif parser.id!=zenodo_id:
            print("Cant load file list.\n Zenodo id doesn't exist please adapt zenode_recod variable to: "+str(parser.id))
        else:
            print("upload:"+str(files))
            parser.update_files(files)
    if "-m" in args:
            i=args.index("-m")+1
            if i<len(args) and os.path.isfile(args[i]):
                meta=parser.read_cff(args[i])
                if 'CI_COMMIT_TAG' in os.environ:
                    version_regex = "^v?([0-9]+(\.[0-9]+)+.*)$"
                    match = re.match(version_regex, os.environ.get('CI_COMMIT_TAG'))
                    meta["metadata"]["version"]= match.group(1)
                parser.change_meta(meta) 
            else:
                print("Cant read meta file"+str(args[i])) 


