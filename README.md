# NFDI4Earth GitLab2Zenodo
This project is part of the [NFDI4Earth](https://www.nfdi4earth.de/).

## Project goals
This project is providing a python3 script to push your GitLab repository to Zenodo.
The meta data for your Zenodo entry will read from the CITATION.cff


## How to install
The gitlab2zenodo.py use the yaml and request libaries to parse the CITATION.cff and use the Zenodo Development API.
Install the requirements via pip:

```$ pip install -r requirements.txt```


### Push docker to gitlab registry
Register at docker logint of yout gitlab host:
```
    docker login registry.git.rwth-aachen.de -u ${tokenname} -p ${token}}
```
Build your dockerfile:
```
docker build -t registry.git.rwth-aachen.de/${gitLabProjectPath} ${path2DockerFile}
```
Push your dockerfile:
```
docker push registry.git.rwth-aachen.de/${gitLabProjectPath}
```
### Setup CI/CD pipeline
Use docker image :
````
image:
   name: busch2luh/gitlab2zenodo
   # if you want to use gitlab registry
   #name: registry.${projectPath}:latest
   #example: registry.git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/gitlab2zenodo:latest
   entrypoint: [""]
   stages:
   -deploy
send-snapshot:
  stage: deploy
  rules:
    - if: $CI_COMMIT_TAG =~ /^v?[0-9]+\.[0-9]+/    
  script:
    - echo $CI_COMMIT_TAG
    - tar  --exclude='CITATION.cff' --exclude='project-description.pdf' -czf ${CI_COMMIT_TAG#v}.tar *
    - python3 /script/gitlab2zenodo.py -i ./CITATION.cff -m  ./CITATION.cff -f ${CI_COMMIT_TAG#v}.tar project-description.pdf CITATION.cff    
````

## How to use
To push your data to Zenodo you have to generate a Zenodo public acess token as enviorment variable ***zenodo_token***. Optionally, the zenodo doi could also handover by ***zenodo_record*** enviorment variable.

The script provides following options:
- \-m .cff upload meat file
- \-f fileA fileB ... uploads files
-  \-c create new Zenodo entry
- \-i reads Zenodo DOI identifier from .cff file

Zenodo subjects and community are set by use .cff references marked with scope name "zenodo cummunity" and "zenodo subject".
The title of "zenodo community" is given by the reference title.
The "zenodo subject" term is given also as  reference title with a URL identifier given by reference URL.
### Continuous Integration/ Continuous Deployment
You could use an automatic update/add of the Zenodo entry following these steps [[Video](https://youtu.be/X8ZUOg0v8iQ)]:
1. Create a variable (***Settings*** -> ***CI*** -> ***Variables***):
    - Key: zenodo_token; Value: ["Your Zenodo Token"](https://zenodo.org/account/settings/applications); check ***mask*** variables option and ***don't protected***.
    - If the Zenodo Id (identifier in your CITATION.cff) already exists, a new draft will created, otherwise a new project (DOI) is created.
2. Publish to Zenodo:
    - Add tag with name v0.1 (***Repository*** -> ***Tags***)
3. The Zenodo Meta data is generated from your CITATION.cff file and uploaded to Zenodo.
    1. Tag for the first time (e.g. v0.1), check the project details at Zenodo and get the pre-registered DOI. 
    2. Adjust the DOI identifiers in your CITATION.cff and the Zenodo badge in the README. 
    3. Tag a second time (e.g. v1.0) and all your data on Zenodo will be updated under the specified DOI.
    4. ***Please check your project on Zenodo carefully (meta and uploaded files) before pressing the "publish" button.***

### Local use
Specify the zenodo_token variable locally:
````
export zenodo_token=${token}
````



## How to cite
@toDO: DOI needs update

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1151481.svg)](https://doi.org/10.5281/zenodo.7586901)

## FAQ
### The meta data are not uploaded
- The CIATIION.cff has some errors: 
    - The zenodo community given by a reference in the .cff file dosen't exists
- Error Message: No bucket link starting first file upload?
    - Some Zenodo internal buck please uplaod some files first and update the meta afterwards
## References

Example:
- Steffen Busch. (2023). NFDI4Earth GitLab Template (0.1). Zenodo. https://doi.org/10.5281/zenodo.7504700

- This work has been funded by the German Research Foundation (NFDI4Earth,
DFG project no. 460036893, https://www.nfdi4earth.de/).


